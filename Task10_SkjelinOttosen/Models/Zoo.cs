﻿using System;
using System.Collections.Generic;
using System.Reflection.Metadata.Ecma335;
using System.Text;

namespace Task10_SkjelinOttosen
{
    public class Zoo
    {
        public string Name { get; set; }
        public List<Animal> Animals { get; set; }
        public int AnimalCount { get; set; }    

        public Zoo(string name) 
        {
            Name = name;
            Animals = new List<Animal>();
        }

        public void AddAnimal(Animal animal)
        {
            Animals.Add(animal);
            AnimalCount++;
        }

        public void PrintAnimalList()
        {
            int count = 0;
            Console.WriteLine("List of animals");
            foreach (Animal animal in Animals)
            {
                Console.WriteLine($"{++count}:\nAnimal: {animal.GetType().Name.ToString()}\n{animal.ToString()}");
            }
            Console.WriteLine($"Animals in total {AnimalCount}\n");
        }
    }
}
