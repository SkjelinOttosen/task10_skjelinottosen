﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace Task10_SkjelinOttosen
{
    public enum Sex
    {
        Male,
        Female
    }
  
    public abstract class Animal : IPutToRestable
    {
        public string Name { get; set; }
        public Sex Sex { get; set; }
    
        public int Weight { get; set; }

        public int Height { get; set; }

        public string Description { get; set; }

        public bool IsAlive { get; set; }

        protected Animal(string name, Sex sex, int weight, int height, string description)
        {
            Name = name;
            Sex = sex;
            Weight = weight;
            Height = height;
            Description = description;
            IsAlive = true;
        }

        public abstract void Eat();
        public abstract void Sleep();
        public virtual void PutToRest()
        {
            
        }
    }
}
