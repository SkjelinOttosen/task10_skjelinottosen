﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task10_SkjelinOttosen 
{
    public abstract class CatFamily : Animal, IClimbable
    {
        public int NumPaws { get; set; }
        public int ClimbRange { get; set; }
        public int ClimbSpeed { get; set; }
     
        public CatFamily(string name, Sex sex, int weight, int height, string description) : base(name, sex, weight, height, description)
        {
            NumPaws = 4;
        }

        public virtual void Climb(int ClimbRange, int ClimbSpeed)
        {
            
        }
    }
}
