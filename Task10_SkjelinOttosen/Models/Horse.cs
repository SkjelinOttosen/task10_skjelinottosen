﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task10_SkjelinOttosen
{
    public class Horse : Animal, IHorseRacingable
    {
        public int NumClover { get; set; }
        public int RaceSpeed { get; set;}
        public int RaceRange { get; set; }

        public bool Winner { get; set; }

        public Horse(string name, Sex sex, int weight, int height, string description) : base(name, sex, weight, height, description)
        {
            NumClover = 4;
        }

        public override void PutToRest()
        {
            IsAlive = false;
        }

        public bool Race(int RaceSpeed, int RaceRange, bool Winner)
        {
            Console.WriteLine();
            Console.WriteLine($"It is very even towards the last turn. Blackie and {Name} are fighting for first place.");
            System.Threading.Thread.Sleep(1000);
            Console.WriteLine("200 meters left");
            System.Threading.Thread.Sleep(2000);
            Console.WriteLine("100 meters");
            System.Threading.Thread.Sleep(1000);
            Console.WriteLine("50 meter");
            System.Threading.Thread.Sleep(1000);
            Console.WriteLine("Finish!");
            System.Threading.Thread.Sleep(1000);

            if (Winner)
            {
                Console.WriteLine($"{Name} won the Prix de l'Arc de Triomphe!");
            }
            else
            {
                Console.WriteLine($"{Name} got disqualified");
            }
            Console.WriteLine();
            return Winner;
        }
        public override void Eat()
        {
            Console.WriteLine("Eats grass and hay.");
        }

        public override void Sleep()
        {
            Console.WriteLine("Sleeping standing up.");
        }

        public override string ToString()
        {
            return $"Name: {base.Name}\nSex: {base.Sex} \nWeight: {base.Weight}kg \nHeight: {base.Height}cm \nDescription: {base.Description}\n";
        }
    }
}
