﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Security.Cryptography.X509Certificates;

namespace Task10_SkjelinOttosen
{
    class Program
    {
        static void Main(string[] args)
        {
            // Creates animals
            Leopard jimmy = new Leopard("Jimmy", Sex.Male, 500, 110, "Jimmy is just Leopard named Jimmy");
            Leopard susan = new Leopard("Susan", Sex.Female,350, 100, "Jimmy´s girlfriends");
            Cat helloKitty = new Cat("Hello Kitty", Sex.Female,4, 24, "Hello Kitty is afictional cat character produced by the Japanese company Sanrio.");
            Cat tomCat = new Cat("Tom Cat", Sex.Male, 8, 40, "Tom Cat is a fictional character in Metro-Goldwyn-Mayer's series of Tom and Jerry theatrical animated short films.");
            Horse jollyJumper = new Horse("Jolly Jumper", Sex.Male,500, 160, "Jolly Jumer is horse character in the Franco - Belgian comics series Lucky Luke");
            Horse abuAsAHorse = new Horse("Abu as a Horse", Sex.Male, 510, 150, "Sbu is transformed into a horse in the movie Aladdin from 1992");

            // Creates the zoo
            Zoo zoo = new Zoo("The Greater Wynnewood Exotic Animal Park");
            zoo.AddAnimal(jimmy);
            zoo.AddAnimal(susan);
            zoo.AddAnimal(helloKitty);
            zoo.AddAnimal(tomCat);
            zoo.AddAnimal(jollyJumper);
            zoo.AddAnimal(abuAsAHorse);

            // Runs until the user exits the program
            while (true)
            {
                try
                {        
                    // Set the Foreground color to white
                    Console.ForegroundColor = ConsoleColor.White;

                    // Console menu for the program
                    Console.WriteLine("Welcome to " + zoo.Name);
                    Console.WriteLine("Press 1 and enter to see all the animals");
                    Console.WriteLine("Press 2 and enter max weight to filter the animals");
                    Console.WriteLine("Press 3 and enter max height to filter the animals");
                    Console.WriteLine("Press 4 and enter to filter by gender");
                    Console.WriteLine("Press 5 and enter to follow a live horse race");
                    Console.WriteLine("Press 9 and enter to clear the screen");
                    
                    // Menu input from the user
                    int input = Int16.Parse(Console.ReadLine());
              
                    // Checks the menu input
                    // Prints the collection
                    if (input == 1)
                    {
                        // Set the Foreground color to dark yellow
                        Console.ForegroundColor = ConsoleColor.DarkYellow;
                        zoo.PrintAnimalList();
                    }
                    // Search for weight
                    else if (input == 2)
                    {
                        Console.WriteLine("Enter maximum weight in kilograms: ");
                        int inputWeight = Int16.Parse(Console.ReadLine());    
                       
                        IEnumerable<Animal> result =
                            from animal in zoo.Animals
                            where animal.Weight < inputWeight
                            select animal;
                      
                        if(result.Count() > 0)
                        {
                            // Set the Foreground color to dark yellow
                            Console.ForegroundColor = ConsoleColor.DarkYellow;
                            Console.WriteLine($"Search criteria: less than {inputWeight}kg");
                            Console.WriteLine($"{result.Count()} found \n");
                            foreach (Animal animal in result)
                            {
                                Console.WriteLine($"{animal}\n");
                            }
                        }
                        else
                        {
                            // Set the Foreground color to dark red
                            Console.ForegroundColor = ConsoleColor.DarkRed;
                            Console.WriteLine("No search results found\n");
                        }             
                    }
                    // Serach for height
                    else if(input == 3)
                    {
                        Console.WriteLine("Enter maximum height in centimeters: ");
                        int inputHeight = Int16.Parse(Console.ReadLine());

                        // Searching through the collection
                        IEnumerable<Animal> result =
                            from animal in zoo.Animals
                            where animal.Height < inputHeight
                            select animal;
                        
                        if (result.Count() > 0)
                        {
                            // Set the Foreground color to dark yellow
                            Console.ForegroundColor = ConsoleColor.DarkYellow;
                            Console.WriteLine($"Search criteria: less than {inputHeight} centimeters");
                            Console.WriteLine($"{result.Count()} found \n");
                            foreach (Animal animal in result)
                            {
                                Console.WriteLine($"{animal}\n");
                            }
                        }
                        else
                        {
                            // Set the Foreground color to dark red
                            Console.ForegroundColor = ConsoleColor.DarkRed;
                            Console.WriteLine("No search results foundC");
                        }
                    }
                    // Search for sex type of animal
                    else if (input == 4)
                    {
                        Console.WriteLine("Enter 0 for male and 1 for female: ");
                        int inputGender = Int16.Parse(Console.ReadLine());
                        Sex sex = (Sex)inputGender;
                        
                        // Searching through the collection
                        List<Animal> animals = zoo.Animals.FindAll(x => x.Sex == sex);

                        // Set the Foreground color to dark yellow
                        Console.ForegroundColor = ConsoleColor.DarkYellow;
                        Console.WriteLine($"Search criteria: sex {sex}");
                        foreach (Animal animal  in animals)
                        {
                            Console.WriteLine(animal.ToString());
                        }
                        Console.WriteLine();
                    }
                    // Live horse race 
                    else if (input == 5)
                    {
                        // Set the Foreground color to dark yellow
                        Console.ForegroundColor = ConsoleColor.DarkYellow;
                        jollyJumper.Race(100, 80, true);
                    }
                    // Animal facts
                    else if (input == 8)
                    {
                        // Set the Foreground color to dark yellow
                        Console.ForegroundColor = ConsoleColor.DarkYellow;
                        tomCat.Climb(50, 10);
                        jimmy.Climb(10, 30);
                    }
                    // Clearing the console
                    else  if (input == 9)
                    {
                        System.Console.Clear();
                    }
                }
                catch (FormatException ex)
                {
                    // Set the Foreground color to dark red
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine(ex.Message);
                    Console.WriteLine();
                }
                catch (Exception ex)
                {
                    // Set the Foreground color to dark red
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine(ex.Message);
                    Console.WriteLine();
                }
            }
        }
    }
}
