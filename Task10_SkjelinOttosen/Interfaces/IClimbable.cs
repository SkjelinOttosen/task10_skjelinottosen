﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task10_SkjelinOttosen
{
    interface IClimbable
    {
        int ClimbRange { get; set; }
        int ClimbSpeed { get; set; }
        public void Climb(int ClimbRange, int ClimbSpeed);
    }
}
