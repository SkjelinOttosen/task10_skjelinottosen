﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task10_SkjelinOttosen
{
    interface IHorseRacingable
    {
        public  int RaceSpeed { get; set; }
        public int  RaceRange { get; set; }
        public bool Winner { get; set; }

        public bool Race(int RaceSpeed, int RaceRange, bool Winner);
    }
}
