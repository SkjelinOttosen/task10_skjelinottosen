A console application that allows the user to learn more about the animals in the Zoo. 
The user can via a console menu select functions such as printing the animals, seeing facts, searching and live horse racing.

The animal objects are made according to principles within object-oriented programming with inheritance and polymorphism. The animal objects also have functionality that are given via interfaces